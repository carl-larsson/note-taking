const initialState = {
  savedNotes: [],
  activeNote: null
};

export default (state = initialState, action) => {
  switch (action.type) {
  case "NOTE_CREATED":
    const noteData = {
      id: action.payload,
      title: "",
      text: ""
    };

    return {
      ...state,
      savedNotes: [...state.savedNotes, noteData]
    };
  case "NOTE_UPDATED":
    const { title, text } = action.payload;

    const savedNotes = state.savedNotes.map((note, index) => {
      if(note.id === state.activeNote) {

        return {
          ...note,
          title,
          text
        };
      }

      return note;
    });

    return {
      ...state,
      savedNotes
    };
  case "NOTE_ACTIVE":
    return {
      ...state,
      activeNote: action.id
    };
  case "NOTE_DELETED":
    const filteredNotes = state.savedNotes.filter(note => note.id !== action.id);

    return {
      ...state,
      savedNotes: filteredNotes
    };
  default:
    return state;
  }
};
