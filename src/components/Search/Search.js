import React, { Component } from "react";
import { connect } from "react-redux";

const mapStateToProps = state => ({
  ...state
});

class Search extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const searchText = event.target.value;

    let listOfNotes = this.props.notes.savedNotes;
    if (searchText === "") return this.props.searchList(listOfNotes, false);

    const filteredList = listOfNotes.filter((note) => {
      if (!note.title && !note.text) return false;

      const noteTitle = note.title.toLowerCase();
      const noteText = note.text.toLowerCase();
      const matchesTitle = noteTitle.includes(searchText.toLowerCase());
      const matchesText = noteText.includes(searchText.toLowerCase());

      return matchesTitle || matchesText;
    });

    return this.props.searchList(filteredList);
  }

  render() {
    const noNotes = this.props.notes.savedNotes.length === 0;

    return (
      <input type="text" placeholder="Sök" onChange={this.handleChange} disabled={noNotes ? "disabled" : undefined} />
    );
  }
}

export default connect(mapStateToProps, null)(Search);