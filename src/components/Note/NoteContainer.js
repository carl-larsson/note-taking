import React, { Component, Fragment } from "react";

import Note from "./Note";
import {connect} from "react-redux";

const mapStateToProps = state => ({
  ...state
});

class NoteContainer extends Component {
  render() {
    const noteProps = this.props.notes;
    const hasActiveNote = !!noteProps.savedNotes.length && noteProps.activeNote;

    const [activeNote] = noteProps.savedNotes.filter((note, index) => {
      return note.id === noteProps.activeNote;
    });

    return (
      <Fragment>
        {activeNote && <Note id={activeNote.id} title={activeNote.title} text={activeNote.text}/> }
        {!hasActiveNote && <span className="note-placeholder">There are no notes</span>}
      </Fragment>
    );
  }
}

export default connect(mapStateToProps, null)(NoteContainer);