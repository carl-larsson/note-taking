import React, { Component } from "react";
import {connect} from "react-redux";

import { createNote, updateNote, selectNote, deleteNote } from "../../actions";
import "./note.scss";
import trashIcon from '../../trash-icon.svg';

const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = dispatch => ({
 createNote: (noteContent) => dispatch(createNote(noteContent)),
 updateNote: (noteContent) => dispatch(updateNote(noteContent)),
 selectNote: (id) => dispatch(selectNote(id)),
 deleteNote: (id) => dispatch(deleteNote(id))
});

class Note extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      title: "",
      text: ""
    };

    this.handleDelete = this.handleDelete.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const newNote = nextProps.id !== this.props.id;
    if (!newNote) return;

    this.setState({
      id: nextProps.id,
      title: nextProps.title,
      text: nextProps.text
    })
  }

  handleDelete() {
    const savedNotes = this.props.notes.savedNotes;

    if (savedNotes.length > 1) {
      const deletedNoteIndex = savedNotes.findIndex(note => note.id === this.props.id);
      const indexToSelect = deletedNoteIndex === 0 ? 1 : deletedNoteIndex - 1;
      const noteToSelect = savedNotes[indexToSelect];
      this.props.selectNote(noteToSelect.id);
    } else {
      this.props.selectNote(0);
    }

    this.props.deleteNote(this.props.id);
  }

  handleChange(event) {
    const stateToChange = event.target.name;

    this.setState({
      [stateToChange]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const formElement = event.target.parentElement;
    const textAreas = formElement.getElementsByTagName("textarea");
    const title= textAreas[0].value;
    const text = textAreas[1].value;

    const noteContent = {
      title,
      text
    };

    this.props.updateNote(noteContent);
  }

  render() {
    return (
      <section className="note">
        <form onSubmit={this.handleSubmit}>
          <textarea className="title" name="title" placeholder="Title" value={this.state.title} onChange={this.handleChange} />
          <textarea className="note-text" name="text" placeholder="Enter text here" value={this.state.text} onChange={this.handleChange} />
          <input className="save-button" type="submit" value="Save" />
          <img className="delete-icon" src={trashIcon} alt="Delete note" onClick={this.handleDelete} />
        </form>
      </section>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Note);