import React, { Component } from "react";
import { connect } from "react-redux";

import { selectNote } from "../../actions";

const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = dispatch => ({
 selectNote: (id) => dispatch(selectNote(id))
});

class NoteListItem extends Component {
  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect() {
    this.props.selectNote(this.props.id);
  }

  render() {
    const isActive = this.props.id === this.props.notes.activeNote;
    const isEmpty = !this.props.title && !this.props.text;
    const createdTime = new Date(this.props.id);

    return (
      <li className={isActive ? "active" : undefined}
        onClick={this.handleSelect}>
        <h4>{isEmpty ? "New note" : this.props.title}</h4>
        <span>{this.props.text}</span>
        <time>{createdTime.getHours()}:{createdTime.getMinutes()}</time>
      </li>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NoteListItem);