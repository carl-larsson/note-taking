import React, { Component } from "react";
import { connect } from "react-redux";

import NoteListItem from "./NoteListItem";
import Search from "../Search/Search";

import "./noteList.scss";

const mapStateToProps = state => ({
  ...state
});

class NoteList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchList: [],
      isSearching: false
    }

    this.searchList = this.searchList.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const itemDeleted = nextProps.notes.savedNotes.length < this.props.notes.savedNotes.length;
    if(itemDeleted) {
      this.setState({
        searchList: [],
        isSearching: false
      })
    }
  }

  searchList(list, isSearching = true) {
    this.setState({
      searchList: list,
      isSearching
    });
  }

  render() {
    const everyNote = this.props.notes.savedNotes;
    const listToShow = this.state.isSearching ? this.state.searchList : everyNote;
    const noSearchHits = this.state.isSearching && !this.state.searchList.length;

    const notes = listToShow.map((noteContent) => {
      const { title, text, id } = noteContent;
      return <NoteListItem title={title} text={text} id={id} key={id} />;
    });

    return (
      <aside className="note-list-wrapper">
        <Search searchList={this.searchList}/>
        {noSearchHits  && <span className="search-message">No matching searches</span> }
        <ul>
          { notes }
        </ul>
      </aside>
    );
  }
}

export default connect(mapStateToProps, null)(NoteList);