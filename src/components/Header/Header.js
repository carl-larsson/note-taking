import React, { Component } from "react";
import { connect } from "react-redux";

import { createNote } from "../../actions";

import "./header.scss";
import plusIcon from '../../plus-icon.svg';

const mapDispatchToProps = dispatch => ({
 createNote: () => dispatch(createNote())
});

class Header extends Component {
  constructor(props) {
    super(props);

    this.handleCreate = this.handleCreate.bind(this);
  };

  handleCreate(event) {
    this.props.createNote();
  }

  render() {
    return (
      <header>
        <div className="header-content">
          <h1>Note taker</h1>

          <div className="create-note" onClick={this.handleCreate}>
            <img src={plusIcon} alt="Add note" />
            <span>New Note</span>
          </div>
        </div>
      </header>
    );
  }
}

export default connect(null, mapDispatchToProps)(Header);