const createNote = (n) => dispatch => {
  const noteId = Date.now();
  dispatch({
    type: "NOTE_CREATED",
    payload: noteId
  });

  dispatch(selectNote(noteId));
};

const updateNote = (noteData) => dispatch => {
  dispatch({
    type: "NOTE_UPDATED",
    payload: noteData
  });
};

const deleteNote = (id) => dispatch => {
  dispatch({
    type: "NOTE_DELETED",
    id
  });
};

const selectNote = (id) => dispatch => {
  dispatch({
    type: "NOTE_ACTIVE",
    id
  });
};

export {
  createNote,
  updateNote,
  deleteNote,
  selectNote
}