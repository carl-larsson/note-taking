import React, {Fragment} from "react";

import Header from "./components/Header/Header";
import NoteContainer from "./components/Note/NoteContainer";
import NoteList from "./components/NoteList/NoteList";

import "./App.scss";

function App() {
  return (
    <Fragment>
      <Header />
      <main>
        <NoteList />
        <NoteContainer />
      </main>
    </Fragment>
  );
}

export default App;
